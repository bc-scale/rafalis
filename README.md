```
  _____            ______      _      _____  _____ 
 |  __ \     /\   |  ____/\   | |    |_   _|/ ____|
 | |__) |   /  \  | |__ /  \  | |      | | | (___  
 |  _  /   / /\ \ |  __/ /\ \ | |      | |  \___ \ 
 | | \ \  / ____ \| | / ____ \| |____ _| |_ ____) |
 |_|  \_\/_/    \_\_|/_/    \_\______|_____|_____/ 
                                                   
```
# Rafalis - Online Image Portfolio

For an example of usage see [the visuals section below](#visuals)

## Description

### Overview

This project is a CMS (Content Management System) to display, manage, and share images. Principal features :
 - Display images with context (camera, author, description, location...)
 - Create collections (image regroupments with possibility to write long texts)
 - Locate images on interactive maps
 - Login system, allowing creators to restrict access to a group of user (private VS public pictures)
 - Manage everything from the web application (creator page).

## Requirements
 - Client specific :
    - `node & npm`
 - Server specific :
    - `Java (JDK 11)`
    - `openssl` to generate rsa keys and permit authentication
 - (optional, for quickstart) `docker` and `docker-compose`


## Usage

### Clone this repository

Make sure to include the 3 submodules.

```bash
$ git clone --recurse-submodules git@gitlab.com:rafalis/rafalis.git
```

### Quickstart
This section should allow you to quickly run an empty version of the application in production mode, allowing you to have an overview.

1) Build the application

    Make sure your `JAVA_HOME` is configured properly (ex: `JAVA_HOME=/home/{user}/.jdks/openjdk-11`):

    ```bash
    $ export JAVA_HOME=/path/to/jdk-11
    ```

    Run the build script

    ```bash
    $ chmod +x quickstart-build.sh
    $ ./quickstart-build.sh
    ```
2) (optional) Configure your credentials

    All the credentials are configurable in an `.env` file. If you just want to try the application, you can just use the `.env-sample` in the next section. For more information on why is there a viewer login, see the wiki below.


    | Variable                  | Default   | Description               |
    | ------------------------- | --------- | ------------------------- |
    | `RAFALIS_DB_PASSWORD`     | `postgres`| **The database password** |
    | `RAFALIS_ADMIN_USR`       | `admin`   | **Creator Login Username**|
    | `RAFALIS_ADMIN_PASSWORD`  | `admin`   | **Creator Login Password**|
    | `RAFALIS_VIP_USR`         | `viewer`  | Privileged viewer Login   |
    | `RAFALIS_VIP_PASSWORD`    | `viewer`  | Privileged viewer password|
    | `RAFALIS_SERVER_URL`      | http://localhost:8080        | The server URL |

2) Launch it

    Get in the `build` directory :

    ```bash
    $ cd build
    ```

    Launch the app with the default credentials (usr: admin, pwd: admin)

    ```bash
    $ docker-compose --env-file env-sample up --build -d
    ```

    You can replace `{env-sample}` with your own file containing your own credentials (recommended).

### Dev mode
For dev mode, you can launch separately the database, the server and the client :

- Database :
    ```bash
    $ cd rafalis-db
    $ docker-compose up -d
    ```

- Server :

    Assuming `JAVA_HOME` is configured correctly (points to `JDK 11`, ex: `JAVA_HOME=/home/{user}/.jdks/openjdk-11`)
    ```bash
    $ cd rafalis-server
    $ ./mvnw compile quarkus:dev
    ```

- Client :
    ```bash
    $ cd rafalis-client
    $ npm ci
    $ ng serve
    ```

The application is now accessible `http://localhost:4200/`.

The server api is listening at `http://localhost:8080/`, with a gui at `http://localhost:8080/q/swagger-ui/`

---

## Wiki

### Authentication and privilege levels
This application provides 3 levels of privileges, which respect the access matrix below :

| Level               |                |                 |               |
|-------------------- | :------------: | :-------------: | :-----------: |
|                     | Public content | Private content | Creator features |
| Anonymous           |       x        |                 |               |
| Logged in as viewer |       x        |        x        |               |
| Logged in as creator|       x        |        x        |      x        |

Content refers to images and collections.
Creator features refer to : modifying images, uploading images, creating and modifying collections, and access to the creator panel.

 > **_NOTE:_** The application will warn you, but if you try to add private images in a public collection, anonymous users will see the private image not loading because they don't have access to it but can still see the collection.


### Adding an image

Upload images and associate them various informations :

| Field             |                                                    |
| ----------------- | -------------------------------------------------- |
| **Title**         | image main title                                   |
| **Author(s)**     | author(s), configurable from the creator panel     |
| **Date**          | date of image (optional)                           |
| **Camera**        | camera, configurable from the creator panel        |
| **Description**   | here you can write some context about the image    |
| **Tags**          | to help you to search your image in the database   |

Moreover, you have 3 radio toggles :

| Toggle            |                                                    |
| ----------------- | -------------------------------------------------- |
| **Hide in browse**| **When *ON*, the image will not be shown in the browse page, but you will still be able to add it in a collection**. I typically use this to add travel pictures, which are not great from a photograph point of view, but which are very nice to have in a collection where the story telling is based on this image.  |
| **Show in homepage** | When *ON*, the image can be randomly selected to be displayed in the home page of the application. |
| **Public** | Whether the image is private or public. See [this section](#authentication-and-privilege-levels) for explanation on the private/public notion.|

### Creating a collection
Once you populated the database with some images, you can create a collection.

From the creator panel, use the `Add collection` button and fill the required fields. Select a thumbnail that will be displayed in the collection page.

Once the collection opened, you can add/modify/remove sections. Three section types are available :
   - **Text** : a text section that will get you a basic text editor.
   - **Single Picture** : a single image. When adding a single picture section, you can choose to automatically add a text section below it containing the center image title.
   - **Carousel** : a carousel containing several images that can be scrolled

Collections can be public or private, just like images.

### Adding a map
For a map, refer to the guide inside the application, available in the creator panel : `(advanced) How do I use maps ?`

## Support
If you have any questions or improvements, feel free to send me an email mathu.chritin@gmail.com, I will be happy to help you or if you have new ideas to present !

## Roadmap
So far, these features are in developpment :
 - "Pin me" feature for images : when viewing an image and editing its attributes, add a pin me button that opens a dialog and lets the user place its image somewhere on a reserved system map, for easy map usage.

These features are in the sandbox :
 - Implement kml to geojson converter in the server, so that we can import directly the file from Google My Maps in the application and parse it.


## Visuals
Some examples of the application in use.

---
### Viewer
![Image viewer](img/img.png "Image viewer")
<figcaption align = "center">Image Viewer</figcaption>

![Image editor](img/img-edit.png "Image editor")
<figcaption align = "center">Image editor</figcaption>

![Image adder](img/img-adder.png "Image adder")
<figcaption align = "center">Image upload</figcaption>

---
### Home page

![Home page](img/home-page.png "Home page")
<figcaption align = "center">Home page</figcaption>

---
### Browser

![Browse page](img/browse-page.png "Browse page")
<figcaption align = "center">Browse page</figcaption>

---
### Collections

![Collections](img/collections.png "Collections")
<figcaption align = "center">Collections</figcaption>


![Collections edition](img/collection-editor.png "Collections Edition")
<figcaption align = "center">Collections edition</figcaption>


![Collections](img/collection-example.png "Collections")
<figcaption align = "center">Collections</figcaption>

---
### Maps

![Maps](img/maps.png "Maps")
<figcaption align = "center">Maps</figcaption>

---
#### Administration

![Creator panel](img/admin.png "Creator panel")
<figcaption align = "center">Creator panel</figcaption>

---

# License

TODO
