#!/bin/sh

set -x

DEST=build
WORKDIR=$(pwd)
echo "\
  _____            ______      _      _____  _____ 
 |  __ \     /\   |  ____/\   | |    |_   _|/ ____|
 | |__) |   /  \  | |__ /  \  | |      | | | (___  
 |  _  /   / /\ \ |  __/ /\ \ | |      | |  \___ \ 
 | | \ \  / ____ \| | / ____ \| |____ _| |_ ____) |
 |_|  \_\/_/    \_\_|/_/    \_\______|_____|_____/ 
                                                   
                                                   "
echo "==== BUILDING CLIENT ===="
cd "$WORKDIR"/rafalis-client/ &&
    npm i --silent
if [ ! -d "./dist" ]; then
    ng build --configuration production
fi
echo "========== OK ==========="

echo "==== BUILDING SERVER ===="
[ -z "$JAVA_HOME" ] && echo "JAVA_HOME is not set. Please set it to point to your JDK-11 directory, like this : 'export JAVA_HOME=/path/to/jdk-11'" &&
    echo "Exiting." &&
    exit 1

command -v openssl >/dev/null 2>&1 || {
    echo >&2 "'openssl' is required to generate rsa keys and build the server. Please install it https://www.openssl.org/ . Aborting."
    exit 1
}
PUBKEYFILE=$WORKDIR/rafalis-server/src/main/resources/pubkey.pem
PRIVKEYFILE=$WORKDIR/rafalis-server/src/main/resources/privkey.pem
cd "$WORKDIR" && [ ! -f "$PUBKEYFILE" ] || [ ! -f "$PRIVKEYFILE" ] &&
    echo "Generating demo rsa keys, remember to generate your owns properly. See rafalis-server/README.md for help." &&
    openssl req -newkey rsa:2048 -new -nodes -keyout "$PRIVKEYFILE" -out csr.pem \
        -subj "/C=/ST=/L=/O=/OU=/CN=" &&
    openssl rsa -in "$PRIVKEYFILE" -pubout >"$PUBKEYFILE" &&
    echo "Generated : $PRIVKEYFILE, $PUBKEYFILE"

cd "$WORKDIR"/rafalis-server/ &&
    ./mvnw package
echo "========== OK ==========="

cd "$WORKDIR" && mkdir -p $DEST/rafalis-client \
    $DEST/rafalis-server/src/main/ \
    $DEST/rafalis-db

cd "$WORKDIR" &&
    cp -r rafalis-client/dist/ rafalis-client/Dockerfile rafalis-client/nginx-default.conf rafalis-client/start.sh $DEST/rafalis-client/ &&
    cp -r rafalis-server/src/main/docker/ $DEST/rafalis-server/src/main/ && cp -r rafalis-server/target/ $DEST/rafalis-server/
cp -r .env-sample $DEST/env-sample

cp docker-compose.yml.template $DEST/docker-compose.yml

printf "\nBuild success \
        \nApplication files located in %s/. \
        \nTo start it, 'cd' into %s and run 'docker-compose --env-file {your-env} up --build -d' where {your-env} is the configuration file (default env-sample) \
        \nThen, open your browser at http://localhost:4200/" $DEST $DEST
